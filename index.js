const express = require("express");
const Config = require('./config/config.js');
const fizzbuzzCheat = require('./fizzbuzzCheat.js');
const fs = require('fs');
const logger = require('./config/logger.js');

const { port,limit } = Config;
const app = express();

app.get("/", (req, res) => {
    logger.info('User entered')
    res.send("<h1>Welcome to Einstein's challenge</h1> <h2>Try /api/fizzbuzz/{random number} to begin</h2>");
});

app.get("/api/fizzbuzz/:id", (req, res) => {
    try {
        logger.info('Beginning FizzBuzz');
        let einstNum = req.params.id;
        if (isNaN(einstNum) || einstNum == null) {
            throw new Error('Enter a number!');
        };
        let fbResponse = fizzbuzzCheat(einstNum,limit);
        let timestamp = new Date().valueOf();
        fbResponse += `, ${timestamp}`;
        fs.appendFile('./fbHistory.csv', `"${fbResponse}" \n`, function (err) {
            if (err) { return logger.error(err) };
        });
        res.send(fbResponse);
        logger.info('FizzBuzz terminated');
    } catch (e) {
        res.send(e.message);
        logger.error(e);
    };
});

app.listen(port, () => logger.info("Listening on port " + port));