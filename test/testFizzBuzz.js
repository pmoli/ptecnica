const assert = require('assert');
const fizzbuzzCheat = require('../fizzbuzzCheat.js')

describe('FizzBuzz Cheat Test', () => {
    it('should return: 4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizzbuzz', () => {
        assert.equal(fizzbuzzCheat(4,12), "4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizzbuzz");
    });
});