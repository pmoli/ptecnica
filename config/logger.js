const winston = require('winston');

const logConfig = {
  'transports': [
    new winston.transports.Console({
      format: winston.format.simple()
    }),
    new winston.transports.File({
      filename: './log/debug.log',
      level: 'debug'
    }),
    new winston.transports.File({
      filename: './log/info.log',
      level: 'info'
    }),
    new winston.transports.File({
      filename: './log/warn.log',
      level: 'warn'
    }),
    new winston.transports.File({
      filename: './log/error.log',
      level: 'error'
    }),
  ],
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf((info) => {
      return `${info.timestamp} :[${info.level}]: ${info.message}`;
    })
  )
};

const logger = winston.createLogger(logConfig);

module.exports = logger;