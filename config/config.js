const port = process.env.PORT || 3000;
const limit = 12;

module.exports = {
    port,
    limit
};