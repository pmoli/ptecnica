const fizzbuzzCheat = (num,length) => {
    let givenNum = num;
    let fbLength = length;
    let fizzbuzz = [];
    let fbList = [];
    for (let i = 0; i < fbLength; i++) {
        let aux = givenNum;
        givenNum++;
        if (aux % (3 * 5) === 0) {
            aux = 'fizzbuzz';
        } else if (aux % 3 === 0) {
            aux = 'fizz';
        } else if (aux % 5 === 0) {
            aux = 'buzz';
        };
        fbList.push(aux);
    }
    fizzbuzz = fbList.toString().replace(/,/g, ", ");
    return fizzbuzz;
};

module.exports = fizzbuzzCheat;